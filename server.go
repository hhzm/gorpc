package main

import (
	"errors"
	"log"
	"net"
	"net/http"
	"net/rpc"
	"time"
)

type Args struct { // this is the message
	A, B int
}
type Quotient struct { // this is the response
	Quo, Rem int
}
type API int // this is the

func (t *API) Multiply(args *Args, reply *int) error {
	*reply = args.A * args.B
	return nil
}
func (t *API) Divide(args *Args, quo *Quotient) error {
	if args.B == 0 {
		return errors.New("divide by zero")
	}
	quo.Quo = args.A / args.B
	quo.Rem = args.A % args.B
	return nil
}

func main() {
	//this is a register function
	arith := new(API)
	rpc.Register(arith)
	rpc.HandleHTTP()
	l, e := net.Listen("tcp", ":12377")
	if e != nil {
		log.Fatal("listen error:", e)
	}
	go http.Serve(l, nil)

	time.Sleep(10 * time.Second)

}
