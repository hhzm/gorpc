package main

import (
	"fmt"
	"log"
	"net/rpc"
)

type Args struct {
	A, B int
}
type Quotient struct {
	Quo, Rem int
}
type API int

func main() {
	serverAddress := "127.0.0.1"
	client, err := rpc.DialHTTP("tcp", serverAddress+":12377")
	if err != nil {
		log.Fatal("dialing:", err)
	}
	args := &Args{7, 3}

	var reply int
	err = client.Call("API.Multiply", args, &reply)
	if err != nil {
		log.Fatal("arith error:", err)
	}
	fmt.Printf("Arith: %d*%d=%d", args.A, args.B, reply)

	/*var reply Quotient
	err = client.Call("API.Divide", args, &reply)
	if err != nil {
		log.Fatal("arith error:", err)
	}
	fmt.Printf("Divide: %d/%d=%d...%d", args.A, args.B, reply.Quo, reply.Rem)
	*/
}
